import java.util.Scanner;

public class Homeworkpart1 {
    public static void main(String[] args) {
        double r, V;
        Scanner scanner = new Scanner(System.in);
        r = scanner.nextDouble();
        V = 4.0 / 3 * Math.PI * Math.pow(r, 3);
        System.out.println("Объем шара равен: " + V);

    }
}
