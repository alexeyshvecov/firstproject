import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        double centimeters, allinch;
        final double INCH = 2.54;

        Scanner scanner = new Scanner(System.in);

        allinch = scanner.nextInt();

        centimeters = allinch * INCH;
        System.out.println(centimeters);

        }
    }

